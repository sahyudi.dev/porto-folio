<!DOCTYPE html>
<html lang="en">

<head>
	<title>Sahyudi - CV Resume</title>
	<meta charset="UTF-8">
	<meta name="description" content="CV Resume Muhamad Sahyudi">
	<meta name="keywords" content="resume, curiculum vitae, Muhamad Sahyudi, Geranting, onepage, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="https://id.linkedin.com/in/muhamad-sahyudi">
	<!-- Favicon -->
	<link href="https://storage.googleapis.com/dicoding-2022.appspot.com/img/sahyudi.jpg" rel="shortcut icon" />

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,400i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="./css/bootstrap.min.css" />
	<link rel="stylesheet" href="./css/font-awesome.min.css" />
	<link rel="stylesheet" href="./css/flaticon.css" />
	<link rel="stylesheet" href="./css/owl.carousel.css" />
	<link rel="stylesheet" href="./css/magnific-popup.css" />
	<link rel="stylesheet" href="./css/style.css" />


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section start -->


	<!-- <header class="header-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4">
					<div class="site-logo">
						<h2><a href="#">Civic</a></h2>
						<p>Enhance your online presence</p>
					</div>
				</div>
				<div class="col-md-8 text-md-right header-buttons">
					<a href="#" class="site-btn">Download CV</a>
					<a href="#" class="site-btn">Discover me</a>
				</div>
			</div>
		</div>
	</header> -->


	<!-- Header section end -->

	<!-- Hero section start -->
	<section class="hero-section spad">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-10 offset-xl-1">
					<div class="row">
						<div class="col-md-6">
							<div class="hero-text">
								<h2 style="font-size: 100px;">Muhamad Sahyudi</h2>
								<p>I’m a software enginner in love with traveling, playing football and discovering new
									worlds and
									cultures.</p>
							</div>
							<div class="hero-info">
								<h2>General Info</h2>
								<ul>
									<li><span>Date of Birth</span>Sept 01, 1997</li>
									<li><span>Address</span>Batam</li>
									<li><span>E-mail</span>sahyudi.dev@gmail.com</li>
									<li><span>Phone </span>+62853 7413 1481</li>
								</ul>
							</div>
						</div>
						<div class="col-md-6">
							<!-- <figure class="hero-image" style="padding-top: -11px"> -->
							<img src="https://storage.googleapis.com/dicoding-2022.appspot.com/img/sahyudi.jpg" class="img-fluid" alt="5" width="450px" height="auto">
							<!-- </figure> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Hero section end -->

	<!-- Social links section start -->
	<div class="social-section">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-10 offset-xl-1">
					<div class="social-link-warp">
						<div class="social-links">
							<!-- <i href=""><i class="fa fa-pinterest"></i></a> -->
							<a href="https://www.linkedin.com/in/muhamad-sahyudi"><i class="fa fa-linkedin"></i></a>
							<a href="https://www.instagram.com/mhmd_sahyudi/"><i class="fa fa-instagram"></i></a>
							<a href="https://www.facebook.com/muhamad.sahyudi01"><i class="fa fa-facebook"></i></a>
							<a href="https://twitter.com/muhamad_sahyudi?s=09"><i class="fa fa-twitter"></i></a>
						</div>
						<h2 class="hidden-md hidden-sm">My Social Profiles</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Social links section end -->

	<!-- Resume section start -->
	<section class="resume-section spad">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-7 offset-xl-2">
					<div class="section-title">
						<h2>Work Experience</h2>
					</div>
					<ul class="resume-list">
						<li>
							<h2>2020-Present</h2>
							<h3>BP BATAM</h3>
							<h4>Web Developer</h4>
						</li>
						<li>
							<h2>2019-2020</h2>
							<h3>CV Media Kreasi Bangsa </h3>
							<h4>Web Developer</h4>
						</li>
						<li>
							<h2>2018-2019</h2>
							<h3>PT Citra Tubindo Tbk</h3>
							<h4>Web Developer</h4>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- Resume section end -->

	<!-- Resume section start -->
	<section class="resume-section with-bg spad">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-7 offset-xl-2">
					<div class="section-title">
						<h2>Education</h2>
					</div>
					<ul class="resume-list">
						<li>
							<h2>2019</h2>
							<h3>Technical Information</h3>
							<h4>Batam State Polytechnic</h4>
							<!-- <p>Sit amet, consectetur adipiscing elit. Sed porttitor orci ut sapien scelerisque viverra.
								Sed trist ique justo nec mauris efficitur, ut lacinia elit dapibus. In egestas elit in
								dap ibus laoreet. Duis magna libero, fermentum ut facilisis id, pulvinar eget tortor.
								Vestibulum pelle ntesque tincidunt lorem, vitae euismod felis porttitor sed. </p> -->
						</li>
						<li>
							<h2>2015</h2>
							<h3>Software Engineering</h3>
							<h4>Vocational High School 4 Batam</h4>
							<!-- <p>Sit amet, consectetur adipiscing elit. Sed porttitor orci ut sapien scelerisque viverra.
								Sed trist ique justo nec mauris efficitur, ut lacinia elit dapibus. In egestas elit in
								dap ibus laoreet. Duis magna libero, fermentum ut facilisis id, pulvinar eget tortor.
								Vestibulum pelle ntesque tincidunt lorem, vitae euismod felis porttitor sed. </p> -->
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- Resume section end -->


	<!-- Contact section end -->

	<!-- Footer section start -->
	<footer class="footer-section">
		<div class="container text-center">
			<div class="copyright">
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy;
				<script>
					document.write(new Date().getFullYear());
				</script> All rights reserved | This template is made
				with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
			</div>
		</div>
	</footer>
	<!-- Footer section end -->


	<!--====== Javascripts & Jquery ======-->
	<script src="./js/jquery-2.1.4.min.js"></script>
	<script src="./js/bootstrap.min.js"></script>
	<script src="./js/owl.carousel.min.js"></script>
	<script src="./js/magnific-popup.min.js"></script>
	<script src="./js/circle-progress.min.js"></script>
	<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
	<script src="./js/main.js"></script>
</body>

</html>